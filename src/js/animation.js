'use strict';
~(function() {
    var
        count = 0,
        easeInOut = Power1.easeInOut,
        bgExit = document.getElementById('bgExit');

    window.init = function() {
        var tl = new TimelineMax({
            onComplete: doLoop
        });
        tl.set('#mainContent', { force3D: false });
        tl.addLabel('frameOne', '+=0.5');
        tl.to('#copyOne', 0.9, { x: 0, rotation: 0.01, ease: easeInOut }, 'frameOne')

        tl.addLabel('frameTwo', '+=1.6');
        tl.add(bgAnimation, 'frameTwo')
        tl.to('#cta', 0.6, { opacity: 1, ease: easeInOut }, 'frameTwo+=2')
    };


    function bgAnimation() {
        var bgtl = new TimelineMax();
        bgtl.to('#bg', .6, { x: -80, rotation: 0.01, force3D: false, ease: Sine.easeOut })
    }

    function doLoop() {
        count++;
        if (count < 3) {

            var tlLoop = new TimelineMax();
            tlLoop.addLabel('loop', '+=1.65');
            tlLoop.to('#copyOne', 0.35, { x: -150, opacity: 0, ease: Sine.easeIn }, 'loop')
            tlLoop.to('#copyTwo', 0.35, { x: -243, opacity: 0, ease: Sine.easeIn }, 'loop')
            tlLoop.to('#cta', 0.25, { x: -300, opacity: 0, ease: Sine.easeIn }, 'loop')
            tlLoop.to('#bg', 0.7, { x: -500 }, 'loop+=1')
            tlLoop.set('#bg', { clearProps: 'all' })
            tlLoop.set(['#copyOne','#copyTwo'], { opacity: 1 }, 'loop+=1.25')
            tlLoop.set('#cta', { x: 0 }, 'loop+=1.25')
            tlLoop.add(init, 'loop+=1.5');
        }

    }

    bgExit.addEventListener('click', function(e) {
        e.preventDefault();
        window.open(window.clickTag);
    });
})();